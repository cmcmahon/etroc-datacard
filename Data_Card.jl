using Random
using Printf

struct Pixel
    row::UInt16 #4 bits
    col::UInt16 #4 bits
    hit::Bool
    TOA::UInt16 #10 bits
    TOT::UInt16 #9 bits
    CAL::UInt16 #10 bits
end
#outer constructor
Pixel(row, col) = Pixel(row, col, 0, 0, 0, 0)

struct Event
    BCID::UInt16 #12 bits
    pixels::Vector{Pixel} #array of pixel objects
    #hits::UInt16 # 9 bits
end

function pixel_hit(row, col)
    return Pixel(row, col, 1, rand(0:(2^10-1)), rand(0:(2^9-1)), rand(0:(2^10-1)))
end

function Event()
    pixels = Vector{Pixel}(undef, 256)
    for i = 0:255
        pixels[i+1] = Pixel((i >> 4), (i & 0x0f))
    end
    BCID = rand(0:(2^12-1))
    return Event(BCID, pixels)
end

function Event(num_hits::Int) #generate event with given number of random hits 
    pixels = Vector{Pixel}(undef, 256)
    for i = 0:255 #fill all pixels
        pixels[i+1] = Pixel((i >> 4), (i & 0x0f))
    end
    BCID = rand(0:(2^12-1))
    #generate hits
    @assert num_hits <= 256
    empty_pixels = Set(0:255)
    for i = 1:num_hits
        pix = rand(empty_pixels)
        setdiff!(empty_pixels, pix) #remove hit pixel from set
        row = pix >> 4
        col = pix & 0x0f
        pixels[pix+1] = pixel_hit(row, col) 
    end
    return Event(BCID, pixels)   
end

function hits(event::Event)
    hit_pixels::Vector{Pixel} = []
    for i = 1:256
        if event.pixels[i].hit
            push!(hit_pixels, event.pixels[i])
        end
    end
    return hit_pixels
end

function Base.show(io::IO, pixel::Pixel)
    print(io, "Pixel (", pixel.row, ",", pixel.col, ") Data:\n")
    print(io, "Hit: ", pixel.hit, "\nTOA: ", pixel.TOA, "\n")
    print(io, "TOT: ", pixel.TOT, "\nCAL: ", pixel.CAL, "\n")
end

function Base.show(io::IO, event::Event)
    print(io, "Event Data:\n")
    print(io, "BCID:", event.BCID, "\n")
    print(io, "hits:", length(hits(event)), "\n")
    print(io, "pixels:\n")
    for h in hits(event)
        print(h)
    end
end

function header_word(event::Event)
    begin_tag::UInt64 = 0x0
    start_sequence::UInt64 = 0x3555555 #26 bits
    bcid::UInt64 = event.BCID
    header::UInt64 = bcid | start_sequence << 12
    return (begin_tag << 38) | header
end

function filler_word()
    begin_tag::UInt64 = 0x3
    fill_sequence::UInt64 = 0x0aaaaaaaaa
    return (begin_tag << 38) | fill_sequence
end

function parity(num::UInt64)
    return isodd(count_ones(num))
end

function trailer_word(hits::Int64)
    begin_tag::UInt64 = 0x2
    start_sequence::UInt64 = 0x5555555
    word::UInt64 = (start_sequence << 9) | hits
    word = (begin_tag << 37) | word
    word = (word << 1) | parity(word)
    return word 
end
#outer constructor
trailer_word(event::Event) = trailer_word(length(hits(event)))

function pixel_data_word(pixel::Pixel)
    toa::UInt64 = pixel.TOA
    tot::UInt64 = pixel.TOT
    row::UInt64 = pixel.row
    col::UInt64 = pixel.col
    begin_tag::UInt64 = 0x1
    word::UInt64 = (toa << 19) | (tot << 10) | pixel.CAL
    word = (row << 33) | (col << 29) | word
    word = (begin_tag << 37) | word
    word = (word << 1) | parity(word)
    return word
end

function datacard(event::Event)
    card::Vector{UInt64} = [header_word(event)]
    pixel_hits = hits(event)
    for h in pixel_hits
        push!(card, pixel_data_word(h))
    end
    push!(card, trailer_word(length(pixel_hits)))
    #push!(card, filler_word()) #don't insert filler for now
    return card
end

function reset_pixelarray!(pixelarray::Vector{Pixel})
    for i = 0:255 #initialize all pixels to zero
        pixelarray[i+1] = Pixel((i >> 4), (i & 0x0f))
    end
end

function read_datacard(card::Vector{UInt64}) #cannot read a datacard with filler
    head::UInt64 = card[1]
    @assert (head >> 12) == 0x3555555 #check that header is correct
    pixels = Vector{Pixel}(undef, 256)
    reset_pixelarray!(pixels)
    num_hits::UInt64 = length(card) - 2
    bcid::UInt16 = head & 0xfff #first 12 bits
    for hit in 2:(num_hits+1)
        word::UInt64 = card[hit]
        @assert !parity(word) #parity check
        @assert (word >> 38) == 0x1 #bits 38-40 must be 0x1
        CAL::UInt16 = (word >> 1)  & 0x3ff #bits 2-11
        TOT::UInt16 = (word >> 11) & 0x1ff #bits 12-20
        TOA::UInt16 = (word >> 20) & 0x3ff #bits 21-30
        col::UInt16 = (word >> 30) & 0x00f #bits 31-34
        row::UInt16 = (word >> 34) & 0x00f #bits 35-38
        pixels[(row << 4) | col] = Pixel(row, col, 1, TOA, TOT, CAL)
    end
    trail::UInt64 = card[end]
    @assert !parity(trail) #parity check
    #bits 11-38 = 0x5555555, bits 39-40 = 0x2
    @assert (trail >> 10) == 0x25555555
    @assert ((trail >> 1) & 0x1ff) == num_hits #bits 2-10 are num_hits
    return Event(bcid, pixels)
end

function read_stream(stream::Vector{UInt64}) #read a continuous stream of words
    num_filler::Int = 0
    num_events::Int = 0
    num_hits::Int = 0
    bcid::UInt16 = 0
    events::Vector{Event} = Event[]
    pixels = Vector{Pixel}(undef, 256)
    reset_pixelarray!(pixels)
    in_event::Bool = false
    for word in stream
        if word == filler_word()
            @assert !in_event
            num_filler::Int += 1
        elseif (word >> 12) == 0x3555555 #tag for a header word
            @assert !in_event
            num_events::Int += 1
            num_hits = 0
            bcid = word & 0xfff #first 12 bits
            in_event = true
        elseif (word >> 38) == 0x1 #tag for a data word
            @assert(in_event) #make sure that we are in the middle of an event (after a header, before a trailer)
            @assert !parity(word) #parity check
            CAL::UInt16 = (word >> 1)  & 0x3ff #bits 2-11
            TOT::UInt16 = (word >> 11) & 0x1ff #bits 12-20
            TOA::UInt16 = (word >> 20) & 0x3ff #bits 21-30
            col::UInt16 = (word >> 30) & 0x00f #bits 31-34
            row::UInt16 = (word >> 34) & 0x00f #bits 35-38
            pixels[(row << 4) | col] = Pixel(row, col, 1, TOA, TOT, CAL)
            num_hits += 1
        elseif (word >> 10) == 0x25555555 #tag for a trailer word
            @assert !parity(word) #parity check
            @assert ((word >> 1) & 0x1ff) == num_hits #bits 2-10 are num_hits
            @assert in_event
            in_event = false
            num_hits = 0
            push!(events, Event(bcid, copy(pixels))) #copy to prevent deleting contents when we reset the array
            bcid = 0
            reset_pixelarray!(pixels)
        else
            @printf("A word in the data stream was not recognized: %d", word)
        end
    end
    @printf("Number of Events: %d\nNumber of filler words: %d\n", num_events, num_filler)
    return events
end

function gen_stream(events::Vector{Event}, num_filler::Int=10)
    num_events::Int = length(events)
    remaining_filler::Int = 0
    stream::Vector{UInt64} = UInt64[]
    for evt in events
        filler_before::Int = rand(0:(num_filler//num_events))
        filler_after::Int = (num_filler//num_events) - filler_before
        for f in 1:filler_before
            push!(stream, filler_word())
            remaining_filler += 1
        end
        append!(stream, datacard(evt)) #add the event
        for f in 1:filler_after
            push!(stream, filler_word())
            remaining_filler += 1
        end
    end
    for f in 1:(num_filler-remaining_filler)
        push!(stream, filler_word())
    end
    return stream
end

tmp_event_1 = Event(1)
tmp_event_2 = Event(2)
evt_array = [tmp_event_1, tmp_event_2]
print(tmp_event_1)
print(tmp_event_2)
println()
for evt in read_stream(gen_stream(evt_array, 10))
    print(evt)
end